#ifndef fooaeswepdhfoo
#define fooaeswepdhfoo

/* $Id$ */

/*
 * This file is part of aeswepd.
 *
 * aeswepd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * aeswepd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aeswepd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <stdint.h>

#define WEP_KEY_LEN 13
#define AES_KEY_LEN 16
#define MAX_WEP_KEYS 16

extern int n_max_keys;
extern int key_map[MAX_WEP_KEYS];

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef VARLIBAESWEPD
#define VARLIBAESWEPD "/var/lib/aeswepd"
#endif

#endif
