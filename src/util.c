/* $Id$ */

/*
 * This file is part of aeswepd.
 *
 * aeswepd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * aeswepd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aeswepd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <sys/socket.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <libdaemon/dlog.h>

#include "util.h"

void print_hex(FILE *f, uint8_t *w, int l) {
    while (l > 0) {
        fprintf(f, "%02x", *(w++));
        l--;
    }
}


int parse_hex(char *s, uint8_t *b, int l) {
    int n = 0;
    char *p;
    int nibble = 0;
    
    for (p = s; *p && l >= 0; p++) {
        int c;

        if (*p >= '0' && *p <= '9')
            c = *p - '0';
        else if (*p >= 'A' && *p <= 'F')
            c = *p - 'A' + 10;
        else if (*p >= 'a' && *p <= 'f')
            c = *p - 'a' + 10;
        else if (*p == ':' || *p == '.')
            continue;
        else
            return -(p-s)-1;

        if (!nibble)
            *b = c << 4;
        else {
            *b |= c & 15;
            b++;
            l--;
            n++;
        }

        nibble = !nibble;
    }

    if (*p)
        return -(p-s)-1;
    
    return n;
}


