/* $Id$ */

/*
 * This file is part of aeswepd.
 *
 * aeswepd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * aeswepd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aeswepd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <mcrypt.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>

#include <libdaemon/dlog.h>

#include "aes.h"
#include "util.h"
#include "aeswepd.h"

#define MAX_CACHE 10

static MCRYPT m = MCRYPT_FAILED;


struct cache_entry {
    uint8_t key[AES_KEY_LEN];
    uint8_t data[AES_KEY_LEN];
    uint8_t result[AES_KEY_LEN];
    time_t timestamp;
};

static struct cache_entry cache[MAX_CACHE];
static int n_cache = 0;

int aes_crypt(uint8_t *key, uint8_t *data, uint8_t *result) {
    int r, j;
    struct cache_entry *e;
    time_t now;
    
    for (j = 0; j < n_cache; j++)
        if (!memcmp(cache[j].key, key, AES_KEY_LEN) && !memcmp(cache[j].data, data, AES_KEY_LEN)) {
            memcpy(result, cache[j].result, AES_KEY_LEN);
            return 0;
        }
    
    if (m == MCRYPT_FAILED) {
        if ((m = mcrypt_module_open("rijndael-128", NULL, "ecb", NULL)) == MCRYPT_FAILED) {
            daemon_log(LOG_ERR, "Failed to open rijndael mcrypt module\n");
            return -1;
        }
    }

    if ((r = mcrypt_generic_init(m, key, AES_KEY_LEN, NULL)) != 0) {
        daemon_log(LOG_ERR, "Failed to encrypt: %s\n", mcrypt_strerror(r));
        return -1;
    }

    memcpy(result, data, AES_KEY_LEN);
    if (mcrypt_generic(m, result, AES_KEY_LEN) != 0) {
        daemon_log(LOG_ERR, "mdecrypt_generic() failed.\n");
        return -1;
    }

    if (mcrypt_generic_deinit(m) != 0) {
        daemon_log(LOG_ERR, "mdecrypt_generic() failed.\n");
        return -1;
    }

    now = time(NULL);
    
    if (n_cache < n_max_keys)
        e = &cache[n_cache++];
    else {
        if (n_cache > n_max_keys)
            n_cache = n_max_keys;

        e = NULL;
        for (j = 0; j < n_cache; j++)
            if (!e || cache[j].timestamp < e->timestamp)
                e = &cache[j];
    }
        
    memcpy(e->key, key, AES_KEY_LEN);
    memcpy(e->data, data, AES_KEY_LEN);
    memcpy(e->result, result, AES_KEY_LEN);
    e->timestamp = now;
    
    return 0;
}

int aes_done(void) {
    if (m != MCRYPT_FAILED) {
        mcrypt_module_close(m);
        m = MCRYPT_FAILED;
    }

    n_cache = 0;

    return 0;
}
