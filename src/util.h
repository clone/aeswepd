#ifndef fooutilhfoo
#define fooutilhfoo

/* $Id$ */

/*
 * This file is part of aeswepd.
 *
 * aeswepd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * aeswepd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aeswepd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <stdio.h>
#include <stdint.h>

#ifndef ETH_ALEN
#define ETH_ALEN 6
#endif

#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif

/*struct hw_addr {
    uint8_t addr[ETH_ALEN];
    };*/

extern struct hw_addr null_ap;

void print_hex(FILE *f, uint8_t *w, int l);
int parse_hex(char *s, uint8_t *b, int l);

#endif
