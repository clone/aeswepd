/* $Id$ */

/*
 * This file is part of aeswepd.
 *
 * aeswepd is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * aeswepd is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aeswepd; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <libdaemon/dlog.h>

#include "keyapi.h"
#include "iwkey.h"
#include "wireless.h"
#include "util.h"
#include "interface.h"

static int n_used_keys = 0;

static int wep_key_add(struct interface *i, const uint8_t w[WEP_KEY_LEN]) {
    struct iwreq req;
    assert(i);
    
    if (n_used_keys >= n_max_keys) {
        daemon_log(LOG_ERR, "Too many keys added!\n");
        return -1;
    }

    memset(&req, 0, sizeof(req));
    strncpy(req.ifr_ifrn.ifrn_name, i->name, IFNAMSIZ);

    req.u.encoding.pointer = (uint8_t*) w;
    req.u.encoding.length = WEP_KEY_LEN;
    req.u.encoding.flags = key_map[n_used_keys++]+1;
    
    if (ioctl(i->fd, SIOCSIWENCODE, &req) < 0) {
        daemon_log(LOG_ERR, "ioctl(SIOCSIWENCODE): %s\n", strerror(errno));
        return -1;
    }

    return 0;    
}

static int wep_key_finish(struct interface *i) {
    struct iwreq req;
    assert(i);

    if (n_used_keys) {
        uint8_t tmp[WEP_KEY_LEN];
        int j;
        
        memset(&req, 0, sizeof(req));
        strncpy(req.ifr_ifrn.ifrn_name, i->name, IFNAMSIZ);
        
        req.u.encoding.pointer = tmp;
        req.u.encoding.length = WEP_KEY_LEN;
        req.u.encoding.flags = (key_map[n_used_keys-1]+1);
        
        if (ioctl(i->fd, SIOCGIWENCODE, &req) < 0) {
            daemon_log(LOG_ERR, "ioctl(SIOCGIWENCODE): %s\n", strerror(errno));
            return -1;
        }
        
        for (j = n_used_keys; j < n_max_keys; j++) {
            memset(&req, 0, sizeof(req));
            strncpy(req.ifr_ifrn.ifrn_name, i->name, IFNAMSIZ);
            
            req.u.encoding.pointer = tmp;
            req.u.encoding.length = WEP_KEY_LEN;
            req.u.encoding.flags = (key_map[j]+1);
            
            if (ioctl(i->fd, SIOCSIWENCODE, &req) < 0) {
                daemon_log(LOG_ERR, "ioctl(SIOCSIWENCODE): %s\n", strerror(errno));
                return -1;
            }
        }
    }
        
    memset(&req, 0, sizeof(req));
    strncpy(req.ifr_ifrn.ifrn_name, i->name, IFNAMSIZ);
    
    req.u.encoding.pointer = NULL;
    req.u.encoding.length = 0;
    req.u.encoding.flags = IW_ENCODE_RESTRICTED;

    if (ioctl(i->fd, SIOCSIWENCODE, &req) < 0) {
        daemon_log(LOG_ERR, "ioctl(SIOCSIWENCODE): %s\n", strerror(errno));
        return -1;
    }

    n_used_keys = 0;
    
    return 0;
}


const struct key_api linux_key_api = {
    open: (void*) interface_open,
    close: (void*) interface_close,
    add: (void*) wep_key_add,
    finish: (void*) wep_key_finish
};
